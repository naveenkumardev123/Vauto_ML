from sentence_transformers import SentenceTransformer, util

from sklearn.metrics.pairwise import cosine_similarity

def Sentence_similarity(model,title, option_list, dict_to_match):
    docs = [dict_to_match[title]] + option_list
    doc_emb = model.encode(docs)
    score = cosine_similarity([doc_emb[0]], doc_emb[1:])
    max_score = max(score[0])
    ind_of_option = score[0].tolist().index(max_score)
    print("Selected for ", title, " is ", docs[ind_of_option+1])
    return [docs[ind_of_option + 1]]

def Sentence_similarity_equipment(model,title, option_list, dict_to_match):
    equipment_selection = []
    equipment_match = {}
    for option in dict_to_match[title]:
        docs = [option] + option_list
        doc_emb = model.encode(docs)
        score = cosine_similarity([doc_emb[0]], doc_emb[1:])
        max_score = max(score[0])
        equipment_match[option] = 'None'
        if max_score > 0.6:
            ind_of_option = score[0].tolist().index(max_score)
            equipment_selection.append(docs[ind_of_option + 1])
            equipment_match[option] = docs[ind_of_option + 1]

    return equipment_selection, equipment_match