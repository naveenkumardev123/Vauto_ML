def send_alert(subject, body,outlook_id= "gowtham.krishna@rap.ventures", 
            outlook_pwd= "Rap@1234", 
            receiver= "gowthamkrishna.rapid@gmail.com, vinsolutionalert@gmail.com", 
            filename= None):
    import configparser
    config = configparser.ConfigParser()
    config.read('ro_bot.config.ini')
    if config["Send_quotes"]["check"] != "yes":
        print(body,"-->", "not sending the mail")
        return 0
    if "vinsolutionalert@gmail.com" not in receiver:
        receiver= receiver+ ", vinsolutionalert@gmail.com"
    import email, smtplib, ssl
    from email import encoders
    from email.mime.base import MIMEBase
    from email.mime.text import MIMEText
    from email.mime.multipart import MIMEMultipart
    outlook_host = "smtp.office365.com"
    outlook_port = "587"
    smtp_server = outlook_host
    port = outlook_port
    sender= outlook_id
    password = outlook_pwd
    
    message = MIMEMultipart("alternative")
    message['Subject']= subject+" (Jeff Wyler Chevrolet Bot)"
    message['From'] = sender
    message['To']= receiver
    text = f"""\
        {body}
        """
    html= f"""\
    <html>
    <body>
        <p> Hi,<br>
        {body}<br>
        <a href="https://rapidautomation.ai/">Rapid Acceleration Partners.</a>
        </p>
    </body>
    </html>  
    """
    part1= MIMEText(text, "plain")
    part2= MIMEText(html, "html")
    if filename == None:
        message.attach(part1)
        message.attach(part2)
        print("No File!")
    else:
        with open(filename, "rb") as attachment:
            part_a= MIMEBase("application", "octet-stream")
            part_a.set_payload(attachment.read())
        encoders.encode_base64(part_a)
        part_a.add_header(
            'Content-Disposition',
            f'attachment; filename={filename}',
        )
        message.attach(part1)
        message.attach(part2)
        message.attach(part_a)
    context = ssl.create_default_context()
    server = smtplib.SMTP(smtp_server, port=port)
    try:
        server.ehlo()
        server.starttls(context= context)
        server.login(sender, password)
        # send mail
        server.sendmail(sender, receiver.split(','), message.as_string())
        print("Mail Sent!")
    except Exception as e:
        print(e)
    finally:
        server.quit()
def show_toast_msg(header= "Notification", text="null"):
    return None
    try:
        from win10toast import ToastNotifier
        toaster= ToastNotifier()
        toaster.show_toast(f"{header}", f"{text}", duration=10, threaded=True)
    except Exception as e:
        print("Not Abel to Show toast", e)
def build_sheet_data(vin):
    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    from global_connect import gc_login, get_build_sheet
    options = Options()
    options.headless = True
    options.add_argument("window-size=1280, 1024")
    js_cmd_click= "arguments[0].click();"
    chrome_path= r"/usr/local/bin/chromedriver"
    browser_gc= webdriver.Chrome(chrome_options=options)
    browser_gc.implicitly_wait(2)
    browser_gc.maximize_window()
    click1 = lambda element: browser_gc.execute_script(js_cmd_click, element)    
    try:
        gc_login(browser_gc, click1)
        return get_build_sheet(browser_gc, click1, vin)
    except Exception as e:
        print(e)
        import traceback
        print(traceback.format_exc())
        return "NO DATA"
def days_between(d1, d2):
    from datetime import datetime
    d1 = datetime.strptime(d1, "%m/%d/%y")
    d2 = datetime.strptime(d2, "%m/%d/%y")
    return int(abs((d2 - d1).days)/30.417)
def persentage_cal(price, percentage):
    try:
        if "$" in str(price) or "," in str(price):
            price= price.replace("$", "").replace(",", '')
    except:
        pass
    _= (int(percentage) * int(price)) // 100
    return int(price)-_
def kill_process():
    import platform
    if platform.system() == "Windows":
        return 0
    import os
    os.system("killall Xvfb")
    os.system("killall Chrome")
    os.system("killall chrome")
    os.system("killall Chromedriver")
    os.system("killall chromedriver")
def ro_bot_date_check(find_date):
    from datetime import date, datetime
    f_date = date(datetime.now().year, datetime.now().month, datetime.now().day)
    l_date = date(datetime.now().year, int(find_date.split("/")[0]), int(find_date.split("/")[1]))
    delta = f_date - l_date
    return delta.days
def max_closed_date():
    from datetime import date, datetime
    max_days= 1000
    check_date= date(datetime.now().year, datetime.now().month, datetime.now().day)
    if check_date.strftime("%A") == "Monday":
        return max_days+1
    else:
        return max_days
# ro_bot_date_check("2/18/21")
# print(persentage_cal("$14126", 10))
# import configparser
# config = configparser.ConfigParser()
# config.read('ro_bot.config.ini')
# from datetime import date
# import os
# today = date.today()
# d1 = today.strftime("%d-%m-%Y")
# input(config["File_receivers"]["mail_ids"])
# send_alert(subject="Test RO Notes", body="Please find attachment", receiver= config["File_receivers"]["mail_ids"], filename= os.path.join("xl", f"{d1}.xlsx"))
# from datetime import datetime
# print(datetime.now().strftime("%m/%d/%y"))
# print(days_between("1-5-19", datetime.now().strftime("%m-%d-%y")))
