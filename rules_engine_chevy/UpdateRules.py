import pandas as pd

RULES = {
    "navigation system": ["navtraffic"],
    "bose premium sound": [
        "Audio system feature, Bose premium speakers with 8-speakers, including sub-woofer",
        "audio system feature, bose performance enhanced premium 10- speaker system",
    ],
    "premium sound": [
        "Audio system feature, Bose premium speakers with 8-speakers, including sub-woofer",
        "audio system feature, bose performance enhanced premium 10- speaker system",
    ],
    "heated seats": ["seats, heated driver and front passenger"],
    "adaptive cruise control": ["adaptive cruise control - advanced"],
    "moon roof": ["sunroof, power, tilt-sliding"],
    "dual skyscape sun roof": ["sunroof, dual skyscape 2-panel power"],
    "bose surround sound": [
        "audio system feature, bose premium 10-speaker system with subwoofer in center console"
    ],
    "premium pkg": [
        "base 3lt leather package (not available with (cxh) lt premium package.)"
    ],
    "texas edition pkg": ["texas edition"],
    "luxury pkg": ["luxury package"],
    "blind-zone": ["blind zone"],
    "dvd": ["dvd"],
    "head-up display": ["head-up display"],
    "max trailering pkg": ["max trailering"],
    "lane keep assist": ["lane keep assist"],
}

df = pd.read_csv("NewRules.csv")
for vauto, xml in zip(df["Vauto"].tolist(), df["XML"].tolist()):
    if not RULES.get(vauto.lower()):
        RULES[vauto.lower()] = []
    RULES[vauto.lower()].append(xml.lower())

for key, value in RULES.items():
    RULES[key] = list(set(RULES[key]))
print("RULES = ", RULES)
