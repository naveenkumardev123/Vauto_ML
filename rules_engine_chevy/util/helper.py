import re

exception2rule = [
    "leather",
    "adaptive cruise control",
    "oversize off-road tires",
    "rear spoiler",
    "roof rack",
]


def find_smallest(vals):
    return_val = vals[0]
    fraction_pattern = re.compile(r"(?P<num>[0-9]+)/(?P<den>[0-9]+)")
    min_val = 999
    for val in vals:
        g = fraction_pattern.search(val)
        numbers = []
        if g:
            f = float(g.group("num")) / float(g.group("den"))
            numbers.append(f)
        for idx, v in enumerate(val):
            if (
                v.isdigit()
                and idx <= (len(val) - 1)
                and val[idx + 1] != "/"
                and val[idx - 1] != "/"
            ):
                numbers.append(int(v))
        if sum(numbers) < min_val:
            min_val = min(sum(numbers), min_val)
            return_val = val
    return return_val
