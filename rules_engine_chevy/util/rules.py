RULES = {
    "navigation system": [
        'audio system, 8" diagonal color touch-screen navigation with chevrolet infotainment',
        "navtraffic",
        "audio system with navigation, am/fm stereo with mp3 compatible cd player",
    ],
    "bose premium sound": [
        "Audio system feature, Bose premium speakers with 8-speakers, including sub-woofer",
        "audio system feature, bose performance enhanced premium 10- speaker system",
    ],
    "premium sound": [
        "Audio system feature, Bose premium speakers with 8-speakers, including sub-woofer",
        "audio system feature, bose performance enhanced premium 10- speaker system",
    ],
    "heated seats": ["seats, heated driver and front passenger"],
    "adaptive cruise control": ["adaptive cruise control - advanced"],
    "moon roof": [
        "sunroof, power, tilt-sliding",
        "sunroof, power, tilt-sliding with express-open and close and wind deflector",
    ],
    "dual skyscape sun roof": ["sunroof, dual skyscape 2-panel power"],
    "bose surround sound": [
        "audio system feature, bose premium 10-speaker system with subwoofer in center console"
    ],
    "premium pkg": [
        "base 3lt leather package (not available with (cxh) lt premium package.)"
    ],
    "texas edition pkg": ["texas edition"],
    "luxury pkg": ["luxury package"],
    "blind-zone": ["blind zone"],
    "dvd": ["dvd"],
    "head-up display": ["head-up display"],
    "max trailering pkg": ["max trailering"],
    "lane keep assist": ["lane keep assist"],
    "dvd system": [
        "entertainment system, rear seat, blu-ray/dvd",
        "entertainment system, rear seat blu-ray/dvd.",
        "audio system with rear seat entertainment and navigation",
        "audio system, am/fm stereo with mp3 compatible cd/dvd player",
    ],
    "parking sensor": ["rear park assist"],
    "backup camera": ["rearview vision camera", "rearview camera system"],
    "air conditioning, rear": ["air conditioning, front manual"],
    "leather": ["leather package"],
    "sun roof (sliding)": ["sunroof, power, tilt-sliding"],
    "cd/mp3": [
        "audio system, chevrolet mylink radio with navigation, am/fm siriusxm stereo with cd player and mp3 playback"
    ],
    "blind spot alert": ["side blind zone alert"],
    "automatic, 6-spd": [
        "transmission, 6-speed automatic, electronically-controlled with overdrive"
    ],
    "parking sensors": ["front park assist rear park assist"],
    "blind-spot alert": ["lane change alert"],
}
