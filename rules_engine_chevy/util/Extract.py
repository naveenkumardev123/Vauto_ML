from xml.dom import minidom as md
import re
# from .wraplogging import logged
import xml.etree.ElementTree as ET



class ExtractValue:
    def __init__(self, xmlfile):
        self.xmlfile = xmlfile
        self.tree = md.parse(self.xmlfile)

    def foreloop(self, firstLoop, secondLoop, component):
        newsitems = []
        for item in self.tree.getElementsByTagName(firstLoop):
            header = item.getElementsByTagName("header")[0]
            if header.firstChild.data.lower() == component.lower():
                for i in item.getElementsByTagName(secondLoop):
                    newsitems.append(i.firstChild.data)
        return newsitems

    def parseXML(self, component):
        newsitems = []
        newsitems.extend(self.foreloop("definition", "category", component))
        newsitems.extend(self.foreloop("factoryOption", "description", component))
        return newsitems

    def parse_style(self, value):
        val = ''
        for item in self.tree.getElementsByTagName("style"):
            for i in value:
                val += item.getAttribute(i) + " "
        return val

    def extractEquip(self):
        newsitems = []
        val = "standard"
        for item in self.tree.getElementsByTagName("genericEquipment"):
            for i in item.getElementsByTagName("category"):
                newsitems.append(i.firstChild.data)
        newsitems.extend(
            self.foreloop("factoryOption", "description", "ADDITIONAL EQUIPMENT")
        )
        for item in self.tree.getElementsByTagName(val):
            for i in item.getElementsByTagName("description"):
                newsitems.append(i.firstChild.data)
        return newsitems

    def dataReq(self):
        output = {
            "Transmission": self.parseXML("TRANSMISSION"),
            "Engine": self.parseXML("Engine"),
            "Trim": self.parse_style(["trim","altBodyType","besttrimname","name"]),
            "Equipment": self.extractEquip(),
            "Drivetrain": self.parse_style("drivetrain"),
        }
        return output

    def best_values(self):
        output = {}
        for item in self.tree.getElementsByTagName("VehicleDescription"):
            output["bestMakeName"] = item.getAttribute("bestMakeName")
            output["bestStyleName"] = item.getAttribute("bestStyleName")
            output["bestTrimName"] = item.getAttribute("bestTrimName")
            output["modelYear"] = item.getAttribute("modelYear")
            output["bestModelName"] = item.getAttribute("bestModelName")
        output["Engine"] = self.engine_value(False)
        return output

    def engine_value(self, justval):
        output = {}
        for item in self.tree.getElementsByTagName("engine"):
            for i in item.getElementsByTagName("cylinders"):
                output["cylinders"] = i.firstChild.data
            for i in item.getElementsByTagName("displacement"):
                for j in i.getElementsByTagName("value"):
                    if j.getAttribute("unit") == "liters":
                        output["displacement"] = j.firstChild.data
        if justval:
            return {
                "cylinders": output["cylinders"],
                "displacement": output["displacement"],
            }
        return f'{output["cylinders"]} cylinders, {output["displacement"]} liters'


def extractEngine(value):
    cylinders = re.sub(
        r"\-cyl|v|l\si[0-9]",
        r"",
        re.findall(r"\d\-cyl|v\d|l\si[0-9]\s", value.lower())[0],
    )
    displacement = re.sub(
        r"l|\sliter", r"", re.findall(r"\d\.\dl|\d\.\d\sliter", value.lower())[0]
    )
    return {"cylinders": cylinders, "displacement": displacement}


def extractTransmission(value):
    speed = re.sub(
        r"\-speed|\-spd", r"", re.findall(r"\d\-speed|\d\-spd", value.lower())[0]
    )
    return {"speed": speed}

# extract_value = ExtractValue(r'C:\Users\praka\Downloads\1C4BJWFG8CL198777.xml')
# print(extract_value.engine_value(True))
# print(extract_value.best_values())
# print(extract_value.dataReq())