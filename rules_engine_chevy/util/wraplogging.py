import logging
from datetime import datetime

logging.basicConfig(filename="isvalid.log", level=logging.INFO)


# decorator
def logged(func):
    def wrapper(*args, **kwargs):
        try:
            logging.info(
                "time {0},started '{1}', parameters : {2} and {3}".format(
                    datetime.now(), func.__name__, args, kwargs
                )
            )
            return func(*args, **kwargs)
        except Exception as e:
            raise e

    return wrapper
