import sqlite3
from typing import List
import json
from .rules import RULES

DB_PATH: str = r"./rules_engine_chevy/db/xml.db"


class DBOps:
    def __init__(self, db_path: str) -> None:
        self.create_xml_table(db_path)
        self.create_selection_table(db_path)
        self.create_input_table(db_path)

    def create_xml_table(self, db_path: str) -> None:
        conn = sqlite3.connect(db_path)
        cur = conn.cursor()
        cur.execute(
            """CREATE TABLE IF NOT EXISTS SOAP (
                    XML text NOT NULL,
                    VIN text NOT NULL
                    );"""
        )
        conn.commit()
        conn.close()

    def create_input_table(self, db_path: str) -> None:
        conn = sqlite3.connect(db_path)
        cur = conn.cursor()
        cur.execute(
            """CREATE TABLE IF NOT EXISTS INPUT (
                    Vin text NOT NULL,
                    Key text NOT NULL,
                    Value text NOT NULL
                    );"""
        )
        conn.commit()
        conn.close()

    def create_selection_table(self, db_path: str) -> None:
        conn = sqlite3.connect(db_path)
        cur = conn.cursor()
        cur.execute(
            """CREATE TABLE IF NOT EXISTS SELECTION (
                    Vin text NOT NULL,
                    Key text NOT NULL,
                    Value text NOT NULL,
                    IsSelected text NOT NULL,
                    Confidence text NOT NULL
                    );"""
        )
        conn.commit()
        conn.close()

    @staticmethod
    def select_query(db_path: str, query: str, value=None) -> List:
        conn = sqlite3.connect(db_path)
        cur = conn.cursor()
        if value == None:
            xml = cur.execute(query).fetchall()
        else:
            xml = cur.execute(query, value).fetchall()
        conn.commit()
        conn.close()
        return xml

    @staticmethod
    def insert_query(db_path: str, query: str, values) -> None:
        conn = sqlite3.connect(db_path)
        conn.execute(query, values)
        conn.commit()
        conn.close()


def rules_extract(name):
    return RULES.get(name)
