import requests, os
from .db_operation import DB_PATH, DBOps
from .wraplogging import logged
from xml.dom import minidom as md
from typing import Callable
import sqlalchemy
import pandas as pd

construct_request: Callable[
    [str], str
] = (
    lambda vin_num: f'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:description7c.services.chrome.com">\
    <soapenv:Header/>\
    <soapenv:Body>\
        <urn:VehicleDescriptionRequest>\
            <urn:accountInfo number="294896" secret="f07be490ce6245e7" country="US" language="en" behalfOf="?"/>\
            <urn:vin>{vin_num}</urn:vin>\
            <urn:switch>IncludeOnlyOEMBuildDataDecode</urn:switch>\
        </urn:VehicleDescriptionRequest>\
    </soapenv:Body>\
    </soapenv:Envelope>'
)


@logged
def soap_post(vin_num: str, save: bool = True, path: str = "../XML") -> str:
    db_connection = sqlalchemy.create_engine(
        "mysql+pymysql://root:root@139.59.46.107:6603/ChromeData"
    )
    URL = "http://services.chromedata.com/Description/7c"
    headers = {"content-type": "text/xml"}
    body = construct_request(vin_num)
    query = f"SELECT XML FROM SOAP WHERE VIN = :vin"
    val = DBOps.select_query(DB_PATH, query, {"vin": vin_num})
    if val:
        return val
    query = f"SELECT XML FROM SOAP WHERE VIN = '{vin_num}'"
    sql_val = pd.read_sql(query, con=db_connection)["XML"].tolist()
    if sql_val:
        return sql_val[0]
    response = requests.post(URL, data=body, headers=headers)
    content = response.content.decode()
    query = "INSERT INTO SOAP (XML, VIN) VALUES(:content, :vin);"
    args_dict = {"content": content, "vin": vin_num}
    DBOps.insert_query(DB_PATH, query, args_dict)
    df = pd.DataFrame()
    df["VIN"] = [vin_num]
    df["XML"] = [response.content.decode()]
    df.to_sql(name="SOAP", if_exists="append", con=db_connection)
    if save:
        if not os.path.isdir(path):
            os.mkdir(path)
        with open(os.path.join(path, f"{vin_num}.xml"), "w") as f:
            xml = md.parseString(response.content)
            xml_pretty_str = xml.toprettyxml()
            f.write(xml_pretty_str)
        return response.content.decode()
    return response.content.decode()
