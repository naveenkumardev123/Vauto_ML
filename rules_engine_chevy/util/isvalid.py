from .db_operation import DBOps, rules_extract, DB_PATH
from .soap import soap_post
from typing import Tuple, Any
from .Extract import ExtractValue, extractEngine, extractTransmission
from .wraplogging import logged
import re, os
from xml.dom import minidom as md
from .helper import find_smallest, exception2rule as e2r


@logged
def isValidSegment(key, value, val):
    if key == "Engine":
        return isValidEngine(value, val)
    elif key == "Transmission":
        return isValidTransmission(value, val)
    elif key == "DriveTrain":
        return isValidDriveTrain(value, val)


@logged
def isValidEngine(value, val):
    validTrim = isValidEngineSub(value, val)
    if validTrim is None or not validTrim:
        if not value["DEFAULT_SELECTED"]:
            return find_smallest(value["Values"]), 0
    return validTrim, 0


# @logged
def isValidEngineSub(value, val):
    try:
        true_false = {}
        for engine_value in value:
            engine = ExtractValue(val).engine_value(True)
            true_false[engine_value] = extractEngine(engine_value) == engine
        true_false_values = list(true_false.values())
        true_false_key = list(true_false.keys())
        if true_false_values.count(True) >= 1:
            return true_false_key[true_false_values.index(True)]
        elif true_false_values.count(True) == 0:
            return
        else:
            """
            TODO: Need to complete this Engine
            """
            true_trims = {
                key: key.split() for key, value in true_false.items() if value == True
            }
            return
    except Exception as e:
        return


@logged
def isValidDriveTrain(value, val):
    try:
        drivetrain_map = {
            "Front Wheel Drive": "FWD",
            "Rear Wheel Drive": "2WD",
            "Four Wheel Drive": "4WD",
            "Rear Wheel Drive": "RWD",
        }
        drivetrain = ExtractValue(val).dataReq()["Drivetrain"]
        return value == drivetrain_map[drivetrain], 100
    except:
        return False, 0


def isvalidtrimrules(trim, value):
    for i in [
        ("sport s "),
        ("limited minivan "),
        ("limited ", "limited x "),
        ("sport ", "sport s "),
        ("laredo ", "laredo e "),
        ("se ", "se plus "),
        ("sxt ", "sxt plus "),
        ("willys wheeler ", "willys wheeler x "),
    ]:
        if len(i) == 1:
            if trim == i[0] and any([trim in val.lower() for val in value["Values"]]):
                for val in value["Values"]:
                    if trim in val.lower() and i[1] not in val.lower():
                        return val, True
        else:
            if trim == i[0] and any([trim in val.lower() for val in value["Values"]]):
                for val in value["Values"]:
                    if trim in val.lower():
                        return val, True
    return None, False


@logged
def isValidTrim(value, val):
    validTrim = isValidTrimSub(value, val)
    print("ValidTrim", validTrim)
    try:
        trim = ExtractValue(val).dataReq()["Trim"].lower() + " "
    except:
        return None, 100
    if isvalidtrimrules(trim, value)[1]:
        return isvalidtrimrules(trim, value)[0], 100
    if validTrim is None or not validTrim:
        if not value["DEFAULT_SELECTED"]:
            return find_smallest(value["Values"]), 0
    return validTrim, 100


@logged
def isValidTrimSub(value, val):
    try:
        true_false = {}
        for trim_value in value["Values"]:
            trim = ExtractValue(val).dataReq()["Trim"].lower()
            true_false[trim_value] = trim + " " in trim_value.lower()
        true_false_values = list(true_false.values())
        true_false_key = list(true_false.keys())
        if true_false_values.count(True) == 1:
            return true_false_key[true_false_values.index(True)]
        elif true_false_values.count(True) == 0:
            return
        else:
            """
            TODO: Need to complete this Trim
            """
            true_trims = {
                key: key.split() for key, value in true_false.items() if value == True
            }
            return
    except Exception as e:
        return


@logged
def isValidTransmission(value, val):
    try:
        transmission = ExtractValue(val).dataReq()["Transmission"]
        xml_speed = extractTransmission(transmission[0])
        value_speed = extractTransmission(value)
        return (
            xml_speed == value_speed
            and any(
                [
                    "manual" in transmission[0].lower() and "manual" in value.lower(),
                    "auto" in value.lower() and "auto" in transmission[0].lower() and "tap" not in transmission[0].lower() and "tap" not in value.lower(),
                    # "auto" in value.lower() and "auto" in transmission[0].lower(),
                    "auto" in value.lower() and "auto" in transmission[0].lower() and "tap" in transmission[0].lower() and "tap" in value.lower()
                ]
            ),
            100,
        )
    except:
        return False, 0


@logged
def soap_interface(vin_num):
    path = "XML"
    query = "SELECT XML FROM SOAP WHERE VIN = :vin"
    val = DBOps.select_query(DB_PATH, query, {"vin": vin_num})
    if not val:
        val = [[soap_post(vin_num, save=True, path="XML")]]
    assert len(val) == 1, "Taking Duplicates"
    if not os.path.isdir(path):
        os.mkdir(path)
    with open(os.path.join(path, f"{vin_num}.xml"), "w") as f:
        xml = md.parseString(val[0][0])
        xml_pretty_str = xml.toprettyxml()
        f.write(xml_pretty_str)
    return val


def get_all_substrings(s):
    length = len(s)
    return list(s[i:j] for i in range(length) for j in range(i + 1, length + 1))


def substring(string):
    values = []
    for i in re.split(r"\s|\-", string.lower()):
        values.append(i)
    return [" ".join(i) for i in get_all_substrings(values) if len(i) >= 2]


@logged
def isValidOther(vin_num, key, value):
    val = soap_interface(vin_num)
    if key.lower() in ["transmission", "driveTrain"]:
        return isValidSegment(key, value, val[0][0])
    val = val[0][0]
    vauto = rules_extract(value.lower())
    val = re.sub(r"\s|\-,\"", " ", val.lower())
    if vauto:
        for i in vauto:
            if i in val:
                return True, 100
    # print(value)
    if value.lower() not in e2r:
        if len(re.split(r"\s|\-", value)) >= 2 and any(
            [i in val.lower() for i in substring(value)]
        ):
            return True, 0
        return False, 0
    if vauto is None:
        return False, 100
    return any([i.lower() in val.lower() for i in vauto]), 100


@logged
def isValidTrimEngine(vin_num, key, value):
    val = soap_interface(vin_num)
    if key.lower() == "trim":
        return isValidTrim(value, val[0][0])
    elif key.lower() == "engine":
        return isValidEngine(value, val[0][0])
    else:
        raise Exception(f"Idk what this is bruh :/. Check this out {key}")


@logged
def isValidMain(vin_num, key, value):
    if key.lower() not in ["trim", "engine"]:
        isvalid = isValidOther(vin_num, key, value)
        query = "INSERT INTO SELECTION (Vin, Key, Value, IsSelected, Confidence) VALUES(:vin_num, :key, :value, :isselected, :confidence);"
        args_dict = {
            "vin_num": vin_num,
            "key": key,
            "value": value,
            "isselected": isvalid[0],
            "confidence": isvalid[1],
        }
        DBOps.insert_query(DB_PATH, query, args_dict)
        return isvalid
    else:
        """
        NOTE:
             * For `TRIM`/`ENGINE` we will update Value with the one we selected
        """
        selection = isValidTrimEngine(vin_num, key, value)
        query = "INSERT INTO SELECTION (Vin, Key, Value, IsSelected, Confidence) VALUES(:vin_num, :key, :value, :isselected, :confidence);"
        args_dict = {
            "vin_num": vin_num,
            "key": key,
            "value": f"{selection}",
            "isselected": True,
            "confidence": True,
        }
        DBOps.insert_query(DB_PATH, query, args_dict)
        return selection
