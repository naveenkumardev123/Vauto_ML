from re import sub
from .db_operation import DBOps, DB_PATH
from .isvalid import isValidMain
from .wraplogging import logged
import ast


@logged
def isValid(vin_num, key, value):
    """
    for all key except trim and engine
        input vin_num: str, key: str, value:str
        EG:
            vin_num: 1C6RR6LT7JS132644
            key: DriveTrain
            value: FWD
        return Bool, Int
    for when key is trim or engine
        input vin_num: str, key: str, value:list
        EG:
            vin_num: 1C6RR6LT7JS132644
            key: Trim
            value: ['SLT Pickup 4D 5 1/2 ft', 'Lone Star Pickup 4D 5 1/2 ft']
        when you have a return match
        return str
        when we dont have anything to return it returns None
        return None
    """
    query = "INSERT INTO INPUT (Vin, Key, Value) VALUES(:vin_num, :key, :value);"
    values = {"vin_num": vin_num, "key": key, "value": str(value)}
    DBOps(DB_PATH).insert_query(DB_PATH, query, values)
    return isValidMain(vin_num, key, value)
