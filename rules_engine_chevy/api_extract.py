from util.api_extract import isValid
import sqlite3
import pandas as pd
from collections import Counter
from tqdm import tqdm


def literal_change(x):
    try:
        import ast

        return ast.literal_eval(x)
    except:
        return x


if __name__ == "__main__":
    con = sqlite3.connect("db/xml.db")
    df = pd.read_sql("SELECT * FROM INPUT WHERE Key='Trim'", con=con)
    df = df[df["Vin"] == "2GCVKPEC2K1132079"]
    # df = df[4:5]
    df.drop_duplicates(inplace=True)
    print(df.head())
    # df = pd.read_excel("RulesEngineChevy.xlsx")
    # df = df[df['WRG'] == "WRG"]
    # out = []
    for idx, row in df.iterrows():
        # for idx, row in df.iterrows():
        try:
            data = literal_change(row[2])
            result = isValid(row[0], row[1], data)[0]
            print(row[0], row[1], data)
            print(result)
            break
        except:
            pass
        # print(row[0], row[1], data, result)
