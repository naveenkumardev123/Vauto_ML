import os
import time
from decorator_time import time_decorater
from test_sentencetransformer import Sentence_similarity,Sentence_similarity_equipment
from rules_match_obj import SentanceMatch,extractEquip

from vauto_obj import VautoAutomation
# from sentence_transformers import SentenceTransformer

class KbbBook(VautoAutomation):
    def condition_based_on_price_matrix(self, title):
        price_condition_check_dict= {"silverado": "Fair", 
                                        "colorado": "Fair", 
                                        "camaro": "Good", 
                                        "tahoe": "Good", 
                                        "suburban": "Good",
                                        "traverse": "Good",
                                        "blazer": "Fair",
                                        "equinox": "Fair",
                                        "volt": "Fair",
                                        "bolt": "Fair",
                                        "volt/bolt": "Fair",
                                        "spark": "Fair",
                                        "cruze": "Fair",
                                        "impala": "Good",
                                        "malibu": "Good",
                                        "trax": "Fair",
                                        }
        condition_to_add_in_notes= price_condition_check_dict.get(self.model.lower(), "Fair")
        for compare_model_name in price_condition_check_dict.keys():
            if compare_model_name in self.model.lower():
                options_list= self.driver.find_elements_by_xpath(f"//span[contains(text(), '{self.VAUTO_BOOK_NAME}')]//parent::div/following-sibling::div//div[@class= 'fields']//div[@class=' optionField']//h2[contains(text(), '{title}')]/following-sibling::div//ul//li//span//span//a")
                options_unchecked= [option for option in options_list if "checked" not in option.get_attribute("class")]
                for unchecked_ele in options_unchecked:
                    if self.percent_reduced_carfax==0 and price_condition_check_dict[compare_model_name] == unchecked_ele.text.strip():
                            print(title, unchecked_ele.text)
                            self.select_option_in_book(title, unchecked_ele.text)
                            condition_to_add_in_notes= price_condition_check_dict[compare_model_name]
                            time.sleep(3)
                    elif "Fair" ==unchecked_ele.text.strip():
                        self.select_option_in_book(title, unchecked_ele.text)
                        condition_to_add_in_notes= "Fair"
                        time.sleep(3)
        return condition_to_add_in_notes

    @time_decorater
    def kbb_trade_process(self,model,model_engine):
        print("............kbb_trade_process........")
        # model= SentenceTransformer('sentence-transformers/multi-qa-MiniLM-L6-cos-v1')
        self.get_selected_not_selected_options("Options_Before_Selection")
        XML_Extract_data = SentanceMatch(self.vin).Extract_from_XML()
        XML_Extract_data.update(extractEquip(self.vin))
        self.data_from_vauto_dict["Dict_From_XML"] = XML_Extract_data
        with open(os.path.join("XML", f"{self.vin}.xml"), "r") as f:
            self.data_from_vauto_dict["XML"]= f.read()
        equipment_done = False
        dict_equipment_match = {}
        for i in range(3):
            fields_to_extract= self.reloading_elems()
            for title in fields_to_extract:
                if "condition" in title.lower():
                    self.condition_based_on_price_matrix(title= title)
                elif title in ("Trim", "Engine", "Transmission", "DriveTrain","Equipment"):
                    options_list, options_checked, options_unchecked= self.get_book_options_and_state(title= title)
                    if (("Engine" in title) or ("Trim" in title)):
                        option_to_select_list= Sentence_similarity(model_engine, title, options_list, self.data_from_vauto_dict["Dict_From_XML"])
                        self.data_from_vauto_dict[title] = option_to_select_list
                    elif "Equipment" in title:
                        if equipment_done==False:
                            option_to_select_list, dict_equipment_match = Sentence_similarity_equipment(model, title, options_list, self.data_from_vauto_dict["Dict_From_XML"])
                            equipment_done = True
                            self.data_from_vauto_dict[title] = option_to_select_list
                    elif (("Transmission" in title) or ("DriveTrain" in title)):
                        option_to_select_list= Sentence_similarity(model,title, options_list, self.data_from_vauto_dict["Dict_From_XML"])
                        self.data_from_vauto_dict[title] = option_to_select_list
                    for option_to_select in option_to_select_list:
                        if option_to_select in options_checked:
                            continue
                        elif option_to_select in options_unchecked:
                            self.select_option_in_book(title= title, option_to_select= option_to_select)
                if len(title)>4 and i==2:
                    break
            self.get_selected_not_selected_options("Options_After_Selection")
        return self.data_from_vauto_dict,dict_equipment_match

            