import functools, time, logging
from helper import show_toast_msg
from datetime import datetime   


for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)

logging.basicConfig(filename='RO_Bot_app.log',
                    format='%(asctime)s %(levelname)s-%(message)s-%(message)s',
                    filemode='a',
                    level = logging.INFO)

def time_decorater(func):
    @functools.wraps(func)
    def time_wraffer(*args, **kwargs):
        start_time =time.perf_counter()
        print(f"Funtion Name: {func.__name__} is starting.")
        logging.info(f"Funtion Name: {func.__name__} is starting.")
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print(f"Time Function started running -> {current_time}")
        logging.info(f"Time Function started running -> {current_time}")
        final_result = func(*args, **kwargs)
        end_time =time.perf_counter()
        run_time = end_time- start_time
        print("$"*100)
        print(f"Finished {func.__name__} in {run_time:.2f} secs")
        logging.info(f"Finished {func.__name__} in {run_time:.2f} secs")
        print("$"*100)
        show_toast_msg(header=func.__name__, text=f"Finished {func.__name__} in {run_time:.2f} secs")
        return final_result
    return time_wraffer

# @time_decorater
# def check():
#     return "HEllO>>"

# check()
