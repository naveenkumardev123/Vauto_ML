import json
import sqlite3
from selenium import webdriver
from helper import persentage_cal
from decorator_time import time_decorater
# from vauto import vauto_login, fill_details, get_trade_total
from time import sleep as wait
import logging, locale

for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)

logging.basicConfig(filename='RO_Bot_app.log',
                    format='%(asctime)s %(levelname)s-%(message)s-%(message)s',
                    filemode='a',
                    level = logging.INFO)
@time_decorater
def get_carfax_window(browser, main_page):
    wait(5)
    print(browser.window_handles)
    logging.info(f"Getting carfax window {browser.window_handles}")
    try:
        assert(len(browser.window_handles)>1)
    except Exception as e:
        print(e)
        browser.find_element_by_xpath("//button[@class=' x-btn-text carfax']").click()
        print(browser.window_handles)

    for window_handle in browser.window_handles:
        if window_handle== main_page:
            pass
        else:
            browser.switch_to_window(window_handle)

@time_decorater
def vechile_history_report(browser, click):
    _= {}
    try:
        err_msg= browser.find_element_by_xpath('//*[@id="ContentPlaceHolder1__ErrorMessage"]')
        _["CarFax Error Msg"]= f"Error: {err_msg}"
        return _        
    except:
        print("Vechile Information is avaliable...")
    import time
    timeout = time.time() + 60*5   # 5 minutes from now
    while len(browser.find_elements_by_xpath("//div[@class='hdrWrap']//div//table//tr"))==0:
        print("Waiting to load the carfax.")
        time.sleep(2)
        if time.time() > timeout:
            break
    print("Outside while loop")
    vechile_history= browser.find_elements_by_xpath("//div[@class='hdrWrap']//div//table//tr")
    for num, his_ele in enumerate(vechile_history):
        key= his_ele.get_attribute("id").strip()
        print(his_ele.text)
        _[key]= his_ele.text.strip()
    browser.save_screenshot("carfax_check.png")
    _["Owners_Count"]= len(browser.find_elements_by_xpath("//table[@id='summaryOwnershipHistoryTable']//th[@class='statCol ownerColumnTitle']"))
    return _

@time_decorater
def vechile_additional_history(browser, click):
    def get_reason(text, temp_dict):
        browser.implicitly_wait(20)
        owner_num= browser.find_element_by_xpath(f"//div[@class= 'details-owner-container']//li[contains(text(), '{text}')]//parent::ul//parent::div//parent::div//parent::div[@class='folderstyle']//div//div//div[contains(text(), 'Owner')]").text
        commments = browser.find_element_by_xpath(f"//div[@class= 'details-owner-container']//li[contains(text(), '{text}')]").text
        temp_dict[f"{field}_owner_{num+1}"]= {"Subtitle": field_sub,"Reason": owner.text, "comments": commments, "owner number": owner_num}
    _={}
    print("$"*80)
    _["Owners_Count"]= len(browser.find_elements_by_xpath("//table[@id='summaryOwnershipHistoryTable']//th[@class='statCol ownerColumnTitle']"))
    _["Send_text"] = "Y"
    # Getting Type of ownership
    for num, owner in enumerate(browser.find_elements_by_xpath("//td[contains(text(), 'Type of owner')]/following-sibling::td")):
        _[f"Vechile_type_owner_{num+1}"]= owner.text
    fields= "Total Loss", "Structural Damage", "Airbag Deployment", "Odometer Check", "Accident / Damage", "Basic Warranty"
    for field in fields:
        temp_dict= {}
        temp_dict.clear()
        field_sub= browser.find_element_by_xpath(f"//table[@id='otherInformationTable']//tr//td//a[contains(text(), '{field}')]/following-sibling::div").text # comments
        print("Title:", field)
        print("Subtitle:", field_sub)
        field_comment_elems= browser.find_elements_by_xpath(f"//table[@id='otherInformationTable']//tr//td//a[contains(text(), '{field}')]//parent::td/following-sibling::td")
        #! Uncomment the below line if not sending the mail if there are more than two owners.
        # if len(field_comment_elems) >2:
        #     _["Error msg"]= f"Owners are more then 2. i.e.. {len(field_comment_elems)} owners."
        #     _["Send_text"] = "N"
        #     return _
        for num, owner in enumerate(field_comment_elems):
            if field =="Accident / Damage":
                if "No Issues Reported" in owner.text:
                    print(f"owner {num+1}:", owner.text)
                    temp_dict[f"{field}_owner_{num+1}"]= {"Subtitle": field_sub,"Reason": owner.text}
                else:
                    print(f"owner {num+1}:", owner.text)
                    try:
                        get_reason(owner.text.capitalize(), temp_dict= temp_dict)
                    except:
                        try:
                            get_reason(owner.text.lower(), temp_dict= temp_dict)
                        except:
                            temp_dict[f"{field}_owner_{num+1}"]= {"Subtitle": field_sub,"Reason": owner.text}
            elif field== "Total Loss" or field== "Structural Damage":
                if "No Issues Reported" in owner.text:
                    print(f"owner {num+1}:", owner.text)
                    temp_dict[f"{field}_owner_{num+1}"]= {"Subtitle": field_sub,"Reason": owner.text}
                else:
                    temp_dict[f"{field}_owner_{num+1}"]= {"Subtitle": field_sub,"Reason": owner.text}
                    # _["Send_text"] = "N"
            else:
                print(f"owner {num+1}:", owner.text)
                temp_dict[f"{field}_owner_{num+1}"]= {"Subtitle": field_sub,"Reason": owner.text}
        _.update(temp_dict)

    print("$"*80)
    browser.implicitly_wait(80)
    return _

def final_price(dict1, amt, config):
    def get_amt(num):
        try:
            locale.setlocale(locale.LC_ALL, 'en_US')
            num= locale.format("%d", int(num), grouping=True)
            return f"${num}"
        except Exception as e:
            print(e)
            return f"${num}"
    amt = str(amt)
    amt = amt.replace("$", "")
    _= ['accidentAirbagRowTableRow', 'genericAccidentRowTableRow', 'minorAccidentRowTableRow', "minorDamageRowTableRow", 'veryMinorAccidentRowTableRow', "moderateDamageRowTableRow"]
    if 'noAccidentRowTableRow' in dict1.keys():
        print(dict1.get("noAccidentRowTableRow", ""))
        return get_amt(amt), 0, dict1.get("noAccidentRowTableRow", "")
    for i in _:
        print(i, _)
        if i in dict1.keys():
            # Minor Damage
            amt= persentage_cal(f"${amt}", config["carfax_ammount"]["accident_report"])
            print(dict1.get(i, ""))
            return get_amt(amt), config["carfax_ammount"]["accident_report"], dict1.get(i, "")
    for key in dict1.keys():
        if "major" in key.lower() and "accident" in key.lower():
            amt = persentage_cal(f"${amt}", config["carfax_ammount"]["accident_report_major"])
            print(dict1.get(key, ""))
            return get_amt(amt), config["carfax_ammount"]["accident_report_major"], dict1.get(key, "")
        if "Accident / Damage_owner" in key:
            try:
                if "no accidents or damage reported" not in dict1[key]["Subtitle"].lower():
                    if "major" in dict1[key]["Subtitle"].lower():
                        print(dict1[key]["Subtitle"])
                        amt = persentage_cal(f"${amt}", config["carfax_ammount"]["accident_report_major"])
                        return get_amt(amt), config["carfax_ammount"]["accident_report_major"], dict1[key]["Subtitle"]
                    else:
                        print("Major Damage")
                        amt = persentage_cal(f"${amt}", config["carfax_ammount"]["accident_report"])
                        return get_amt(num= amt), config["carfax_ammount"]["accident_report"], "Major Damage"
            except:
                pass
    print("No accidents or damage reported to CARFAX")
    return get_amt(amt), 0, "No accidents or damage reported to CARFAX"

# print(final_price({}, "$2132", None))

# import pandas as pd
# conn = sqlite3.connect('ro_bot.db')
# df_opt = pd.read_sql_query(f"SELECT * FROM Option_Selection WHERE VIN_NUM='1GNEVJKW4KJ193160';", con=conn)
# print(df_opt["AllChoices"])


# df = pd.read_sql_query(f"SELECT * FROM Option_Selection", con=conn)
# df.to_csv("option_selection.csv")
# print(list(set(df["AllChoices"].to_list())))
# print(df.loc[(df['Title'] == "Equipment") & (df['Options'] == "head-up display")])
# df = pd.read_sql_query(f"SELECT * FROM VIN_XML WHERE VIN_NUM= '1GKS1CKJ6GR366665';", con=conn)
# print(df["XML"].to_list()[0])

# df = pd.read_sql_query(f"SELECT * FROM VIN_XML;", con=conn)
# df.to_csv("vin_xml.csv")
# input(df.columns)
# df = pd.read_sql_query(f"SELECT * FROM Option_Selection WHERE VIN_NUM = '1GKS1CKJ6GR366665';", con=conn)
# input(df)

# for num, i in enumerate(df["VIN_NUM"].to_list()):
#     if num==20:
#         break
#     if num< 10:
#         continue
#     browser_vauto= webdriver.Chrome()
#     browser_vauto.implicitly_wait(80)
#     browser_vauto.maximize_window()
#     try:
#         js_cmd_click= "arguments[0].click();"
#         vin = i
#         click1 = lambda element: browser_vauto.execute_script(js_cmd_click, element)
#         vauto_page= vauto_login(browser_vauto, click1)
#         fill_details(browser_vauto, click1, vin, "10000")
#         vauto_main_page= browser_vauto.current_window_handle
#         click1(browser_vauto.find_element_by_xpath("//button[@class=' x-btn-text carfax']"))
#         wait(4)
#         get_carfax_window(browser= browser_vauto, main_page= vauto_main_page)
#         msg= vechile_history_report(browser= browser_vauto, click= click1)
#         if msg.get("CarFax Error Msg", "")=="":
#             with open(f"{vin}.json", "w") as f:
#                 json.dump(vechile_additional_history(browser= browser_vauto, click= click1), f)
#         else:
#             with open(f"{vin}.json", "w") as f:
#                 json.dump(msg, f)
#     except Exception as e:
#         print(e)
#         pass
#     finally:
#         browser_vauto.quit()
