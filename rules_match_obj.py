import os
from rules_engine_chevy.util.isvalid import soap_interface
from xml.dom import minidom as md

class SentanceMatch:

    def __init__(self, vin):
        self.vin= vin

    def Extract_from_XML(self):
        xml = soap_interface(self.vin)[0][0]
        os.makedirs("XML", exist_ok= True)
        with open(os.path.join("XML", f"{self.vin}.xml"), "w") as f:
            xml = md.parseString(xml)
            xml_pretty_str = xml.toprettyxml()
            f.write(xml_pretty_str)

        with open(os.path.join("XML", f"{self.vin}.xml"), 'r') as f:
            data = f.read().lower()
            data = data.replace('""', '"')
            data = data.replace('&quot', '"')
            data = data.replace('gasoline fuel', ' ')


        data_from_xml_dict = {"Trim": '', "Engine": '', "Transmission": '', "DriveTrain": ''}
        int_i = 0
        for i in data.split('\n'):
            if 'besttrimname' in i:
                data_from_xml_dict["Trim"] += i.split('besttrimname="')[1].split('"')[0]
            if 'beststylename' in i:
                data_from_xml_dict["Trim"] += " " + i.split('beststylename="')[1].split('"')[0]
            if 'altstylename' in i:
                data_from_xml_dict["Trim"] += " " + i.split('altstylename="')[1].split('"')[0]
            if 'altbodytype' in i:
                data_from_xml_dict["Trim"] += " " + i.split('altbodytype="')[1].split('"')[0]
            if 'enginetype' in i:
                if i.split('>')[1].split('<')[0][0] ==  'v':
                    data_from_xml_dict["Engine"] += i.split('>')[1].split('<')[0]
                else:
                    data_from_xml_dict["Engine"] += 'v'+i.split('>')[1].split('<')[0]
            if "fueltype" in i:
                data_from_xml_dict["Engine"] += " " + i.split('>')[1].split('<')[0]
            if 'value unit="l' in i:
                data_from_xml_dict["Engine"] += " " + i.split('>')[1].split('<')[0] + " " + i.split('unit="')[1].split('"')[0]
            if "fuelcapacity unit" in i:
                data_from_xml_dict["Engine"] += " " + i.split('=')[-1].strip('">/') + " " + i.split('unit="')[1].split('"')[0]  
            if '>transmission' in i:
                print(i)
                print(data.split('\n')[int_i+1])
                if i.split('transmission')[1].split('<')[0].strip(',:= ') == "":
                    data_from_xml_dict["Transmission"] += data.split('\n')[int_i+1].split('</')[0].split('>')[1].strip(',:= ')
                else:
                    data_from_xml_dict["Transmission"] += i.split('transmission')[1].split('<')[0].strip(',:= ')

            if 'drivetrain=' in i:
                data_from_xml_dict["DriveTrain"] += i.split('drivetrain="')[1].split('"')[0].strip(',:= ')
            if 'marketclass' in i:
                data_from_xml_dict["DriveTrain"] += " " + i.split('>')[1].split('<')[0]
            int_i += 1

        data_from_xml_dict["Trim"] = (' '.join(dict.fromkeys(data_from_xml_dict["Trim"].split())))
        data_from_xml_dict["Engine"] = (' '.join(dict.fromkeys(data_from_xml_dict["Engine"].split())))
        data_from_xml_dict["Transmission"] = (' '.join(dict.fromkeys(data_from_xml_dict["Transmission"].split())))
        data_from_xml_dict["DriveTrain"] = (' '.join(dict.fromkeys(data_from_xml_dict["DriveTrain"].split())))

        print("data_from_xml_dict:: ", data_from_xml_dict)

        return data_from_xml_dict


def extractEquip(vin_no):
    tree = md.parse(os.path.join("XML", f"{vin_no}.xml"))
    newsitems = []
    val = "standard"
    for item in tree.getElementsByTagName("genericEquipment"):
        for i in item.getElementsByTagName("category"):
            newsitems.append(i.firstChild.data)
    newsitem = []
    for item in tree.getElementsByTagName("factoryOption"):
        header = item.getElementsByTagName("header")[0]
        if header.firstChild.data.lower() == "ADDITIONAL EQUIPMENT".lower():
            for i in item.getElementsByTagName("description"):
                newsitem.append(i.firstChild.data)
    newsitems.extend(newsitem)
    for item in tree.getElementsByTagName(val):
        for i in item.getElementsByTagName("description"):
            newsitems.append(i.firstChild.data)
    print({'Equipment':newsitems})
    return {'Equipment':newsitems}
