import pandas as pd
import traceback
from VIN_LIST import vin_list
from kbb_odj import KbbBook
from vauto_obj import VautoAutomation
from sentence_transformers import SentenceTransformer
# from vauto_GC import vauto_login, fill_details, get_trade_total
# from vauto_sent_analy import vauto_login, fill_details, get_trade_total
import traceback
# from api_extract import extract_additionalvalues
# from glob al_connect import gc_login, get_build_sheet
from selenium import webdriver
from openpyxl import load_workbook, Workbook
import time
import configparser, json
# from carfax import get_carfax_window, vechile_history_report, vechile_additional_history, final_price
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options

def write_data_to_excel_review(vin, title, option, selection, default_value="", GLOBAL_BUILD_SHEET_DATA= ""):
    _= {}
    from openpyxl import load_workbook
    filename= r"C:\Users\Intel\Downloads\vauto_test.xlsx"
    filename= r"vauto_data_GC.xlsx"
    try:
        wb = load_workbook(filename= filename)
        ws = wb.worksheets[0]
    except FileNotFoundError:
        wb= Workbook()
        ws = wb.active
        ws["A1"]= "VIN"
        ws["B1"]= "Title"
        ws["C1"]= "option"
        ws["D1"]= "Selection"
    print(ws.max_row)
    row_num= ws.max_row+1
    ws[f"A{row_num}"]= str(vin)
    ws[f"B{row_num}"]= str(title)
    ws[f"C{row_num}"]= str(option)
    ws[f"D{row_num}"]= str(selection)
    ws[f"E{row_num}"]= str(GLOBAL_BUILD_SHEET_DATA)
    for name in ("A", "B", "C", "D"):
        ws.column_dimensions[name].width = 20
    wb.save(filename)
    wb.close()
    try:
        df= pd.read_excel(filename, engine= "openpyxl")
        df.to_csv("vauto_test.csv", index=False)
    except:
        pass

def write_data_to_excel(dict_equipment_match, dict, default_value=""):
    _= {}
    # import openpyxl
    # # filename= r"C:\Users\Intel\Downloads\vauto_data.xlsx"
    # filename= "vauto_test_data_GC(jan-31,2021).xlsx"
    # try:
    #     wb = load_workbook(filename= filename)
    #     ws = wb.worksheets[0]
    # except FileNotFoundError:
    #     wb= Workbook()
    #     ws = wb.active
    #     ws["A1"]= "VIN"
    #     ws["B1"]= "GLOBAL_CONNECT_RESPONSE"
    #     ws["C1"]= "Trim"
    #     ws["D1"]= "Engine"
    #     ws["E1"]= "Transmission"
    #     ws["F1"]= "DriveTrain"
    #     ws["G1"]= "Certified"
    #     ws["H1"]= "Exterior Color"
    #     ws["I1"]= "Equipment"
    #     ws["J1"]= "before_image_path"
    #     ws["K1"]= "selected_options"
    #     ws["L1"]= "image_path"
    #     ws["M1"]= "Price"
    #     ws["N1"]= "Options_Before_Selection"
    #     ws["O1"]= "Options_After_Selection"
    #     ws["P1"]= "XML"
    #     ws["Q1"]= "Dict_From_XML"
    #     ws["R1"]="Dict_From_XML_Equipment"
    dynamo_db_dict = {"VIN": "", "GLOBAL_CONNECT_RESPONSE": "", "Trim": "", "Engine": "", "Transmission": "", "DriveTrain": "", 
    "Certified": "", "Exterior Color": "", "Equipment": "", "before_image_path": "", "selected_options": "", "image_path": "", 
    "Price": "", "Options_Before_Selection": "", "Options_After_Selection": "", "XML": "", "Dict_From_XML": "", "Dict_From_XML_Equipment": ""}
    dynamo_db_dict["VIN"]= str(dict.get("VIN", ""))
    dynamo_db_dict["GLOBAL_CONNECT_RESPONSE"]= str(dict.get("GLOBAL_CONNECT_RESPONSE", default_value))
    dynamo_db_dict["Trim"]= str(dict.get("Trim", default_value))
    dynamo_db_dict["Engine"]= str(dict.get("Engine", default_value))
    dynamo_db_dict["Transmission"]= str(dict.get("Transmission", default_value))
    dynamo_db_dict["DriveTrain"]= str(dict.get("DriveTrain", default_value))
    dynamo_db_dict["Certified"]= str(dict.get("Certified", default_value))
    dynamo_db_dict["Exterior Color"]= str(dict.get("Exterior Color", default_value))
    dynamo_db_dict["Equipment"]= str(dict.get("Equipment", default_value))
    dynamo_db_dict["before_image_path"]= str(dict.get("before_image_path", default_value))
    dynamo_db_dict["selected_options"]= str(dict.get("selected_options", default_value))
    dynamo_db_dict["image_path"]= str(dict.get("image_path", default_value))
    dynamo_db_dict["Price"]= str(dict.get("Price", default_value))
    dynamo_db_dict["Options_Before_Selection"]= str(dict.get("Options_Before_Selection", default_value))
    dynamo_db_dict["Options_After_Selection"]= str(dict.get("Options_After_Selection", default_value))
    dynamo_db_dict["XML"]= str(dict.get("XML", default_value))
    dynamo_db_dict["Dict_From_XML"]= str(json.dumps(dict_equipment_match))
    import requests
    # print(dynamo_db_dict)
    url = 'https://qpe1dunl74.execute-api.us-east-1.amazonaws.com/ADF_Api/adf_resource'
    x = requests.post(url, json = dynamo_db_dict)
    print(x.text)
        # ws["F1"]= "Confirmation"
        # ws["L1"]= "Insurance"
    # print(ws.max_row)
    # row_num=ws.max_row+ 1
    
    # ws[f"A{row_num}"]= str(dict.get("VIN", ""))
    # ws[f"B{row_num}"]= str(dict.get("GLOBAL_CONNECT_RESPONSE", default_value))
    # ws[f"C{row_num}"]= str(dict.get("Trim", default_value))
    # ws[f"D{row_num}"]= str(dict.get("Engine", default_value))
    # ws[f"E{row_num}"]= str(dict.get("Transmission", default_value))
    # ws[f"F{row_num}"]= str(dict.get("DriveTrain", default_value))
    # ws[f"G{row_num}"]= str(dict.get("Certified", default_value))
    # ws[f"H{row_num}"]= str(dict.get("Exterior Color", default_value))
    # ws[f"I{row_num}"]= str(dict.get("Equipment", default_value))
    # ws[f"K{row_num}"]= str(dict.get("selected_options", default_value))
    # ws[f"M{row_num}"]= str(dict.get("Price", "Price not found"))
    # ws[f"N{row_num}"]= str(dict.get("Options_Before_Selection", default_value))
    # ws[f"O{row_num}"]= str(dict.get("Options_After_Selection", default_value))
    # ws[f"P{row_num}"]= str(dict.get("XML", default_value))
    # ws[f"Q{row_num}"]= str(dict.get("Dict_From_XML", default_value))
    # ws[f"R{row_num}"]= str(json.dumps(dict_equipment_match))



    # ws[f"A{row_num}"]= str(dict.get("Customer_Name", ""))
    # ws[f"D{row_num}"]= str(dict.get("Condition", default_value))
    # ws[f"C{row_num}"]= str(dict.get("Sold_Month", ""))
    # ws[f"D{row_num}"]= str(dict.get("odometer", default_value))
    # ws[f"E{row_num}"]= str(dict.get("car_fax", default_value))
    # ws[f"G{row_num}"]= str(dict.get("AllChoices", default_value))
    # ws[f"R{row_num}"]= str(dict.get("Text_msg", default_value))

    # for name in ("A", "B", "C", "D", "E", "F", "G","H","I","J","K","L","M", "N", "O", "P"):
    #     ws.column_dimensions[name].width = 20
    # for value_name in ["before_image_path", "image_path"]:
    #     try:
    #         dict[value_name]
    #         # dict["image_path"]
    #         if "screenshot" in dict[value_name]:
    #             img = openpyxl.drawing.image.Image(dict[value_name])
    #             img.height= 200
    #             img.width= 350
    #             if value_name== "before_image_path":
    #                 column_name= "J"
    #             else:
    #                 column_name= "L"
    #             img.anchor = f'{column_name}{row_num}'
    #             ws.add_image(img)
    #             ws.row_dimensions[row_num].height = 150
    #             ws.column_dimensions[column_name].width = 50
    #     except:
    #         pass
    # wb.save(filename)
    # wb.close()


# def car_fax(vin_num, final_dict={}):
#     config = configparser.ConfigParser()
#     config.read('ro_bot.config.ini')
#     chrome_path= "/home/gowtham/Documents/Rap_Projects/Ro_Bot/vinsolutions_ro/chromedriver"
#     options = Options()
#     options.headless = True
#     options.add_argument("window-size=1280, 1024")
#     browser_carfax= webdriver.Chrome(chrome_options= options, executable_path = chrome_path)
#     try:
#         js_cmd_click= "arguments[0].click();"
#         click2 = lambda element: browser_carfax.execute_script(js_cmd_click, element)
#         browser_carfax.get(f"https://www.carfaxonline.com/cfm/Display_Dealer_Report.cfm?partner=VAU_0&UID=C574301&vin={vin_num}")
#         car_fx= {}
#         hist= vechile_history_report(browser= browser_carfax, click= click2)
#         add_hist= vechile_additional_history(browser= browser_carfax, click= click2)
#         car_fx.update(hist)
#         car_fx.update(add_hist)
#         final_dict["car_fax"]= car_fx
#         print(final_dict["car_fax"])
#         print(final_price(final_dict["car_fax"], "29124", config))
#     except Exception as e:
#         print(traceback.format_exc())
#         browser_carfax.save_screenshot("error.png")
#         print("Carfax Issue:", e)
#         final_dict["car_fax"]=={"Send_text": "Y"}
#     finally:
#         browser_carfax.close()


config = configparser.ConfigParser()
config.read('ro_bot.config.ini')
chrome_path= r"/usr/local/bin/chromedriver"
options = Options()
options.headless = True
options.add_argument("window-size=1280, 1024")
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
browser_vauto= webdriver.Chrome(chrome_options=options) if config["Select_Browser"]["vauto_name"]== "Chrome" else webdriver.Firefox()
browser_vauto.implicitly_wait(2)
browser_vauto.maximize_window()
# vin_list= ["1C4RJJBG0M8126001"]
#             "1GCRYDED8KZ222187"]

flag = 0
model_engine = SentenceTransformer('sentence-transformers/bert-base-nli-mean-tokens')
model = SentenceTransformer('sentence-transformers/multi-qa-MiniLM-L6-cos-v1')
for vin_num in vin_list:
    kbb_vauto_operation_obj= KbbBook(driver= browser_vauto, vin= vin_num, odometer=100, config=config, model= "Silverado 1500", 
                        percent_reduced_carfax= 0)
    if flag == 0:
        kbb_vauto_operation_obj.vauto_login()

    kbb_vauto_operation_obj.fill_details()
    kbb_vauto_operation_obj.expand_book()
    data_from_vauto_dict,dict_equipment_match = kbb_vauto_operation_obj.kbb_trade_process(model, model_engine)
    kbb_vauto_operation_obj.get_price()
    kbb_vauto_operation_obj.data_from_vauto_dict
    kbb_vauto_operation_obj.data_from_vauto_dict["VIN"]= kbb_vauto_operation_obj.vin
    write_data_to_excel(dict_equipment_match, dict= kbb_vauto_operation_obj.data_from_vauto_dict)
    flag = 1
    # time.sleep(2)
kbb_vauto_operation_obj.close_VAuto_site()





# vauto_test("1GNSCJKC2FR625217", "123343")
# with open("test_vauto.json", "r") as f:
#     data= json.load(f)
# write_data_to_excel(data)

# 3GCPCREC8GG218786-- Minor Damage

# check_vins= ["1GNSCBKCXFR608431","1GCGSCEN9K1306447","1GCGSCEN0K1109392", "2GNAXNEV4K6294649", "1GNSCJKC2FR625217", "1GCGSDEN5K1329472", "1GKS2HKJ9GR438968", "1GNSCBE06BR115163", "1GNKRLED2BJ221728",
    # "3GNAXKEVXKL127402","1GNERGKW4KJ164068","1G1RD6S56JU130522","1GNERKKW7JJ275165","1GNERGKW6KJ146493","1GNERJKW5KJ291165", "1G1FW6S04J4133372","1G1YY22G825105126", "1GNERHKW2KJ292895","1GNSCAKC0KR184682", "1GNSKHKC2JR325102",
    # "1GNKRGKD0GJ348206","KL4CJASB8JB653030","1GNERKKW5LJ106099","1GCUYGEL3MZ102660"]
# check_vins=["3GCPCSE09CG254851", "1GNERKKW5KJ151848", "1GCGSBEA4M1147072"]
# vin= "1GNFK13077J292537"
# vauto_test(vin, "0")
# exit()
# vin= "3GCPCREC8GG218786"
# vehicle_description= "Condition: "+ "condition_to_add_in_notes" + "\n" + "Closed Date: " + "closed_date" + "\n" + "Carfax Msg: " + "carfax_msg"
# vauto_test(vin, "0")

# car_fax(vin)

# df= pd.read_excel(r"C:\Users\Intel\Desktop\ro_vins_to_collect.xlsx", engine= "openpyxl")
# check_vins= df["VIN"].to_list()
# print(check_vins[-1])

# df= pd.read_csv("vin_Data_to_run.csv")
# check_vins= df["VIN"].to_list()


# df= pd.read_csv(r"/home/gowtham/Documents/Rap_Projects/Ro_Bot/vinsolutions_ro/price_info_test.csv")#, engine= "openpyxl")
# check_vins= list(df.Vin_number.unique())
# for num, vin in enumerate(check_vins):
#     if num==100:
#         print("100 Entries are completed, Waiting for input:")
#     try:
#         df= pd.read_excel("vauto_test_data_GC(may16,2021).xlsx", engine= "openpyxl")
#         vin_list= df["VIN"].to_list()
#     except FileNotFoundError:
#         vin_list= []
#     print("VIN:", vin)
#     if vin in vin_list:
#         print(vin, "--->", "Already Completed...")
#         continue
#     try:
#         print(f"Running VIN: {vin}")
#         vauto_test(vin, "0")
#         # input("Waiting")
#     except Exception as e:
#         print(e)

# print("VIN:", vin)

#! sample
# from bot_sql import create_connection
# from api_extract import soap_post
# df= pd.read_excel("vauto_test_data_GC(may16,2021).xlsx", engine= "openpyxl")
# vin_list= df["VIN"].to_list()
# df= pd.read_sql(f"SELECT * from VIN_XML", con =create_connection("ro_bot.db")) 
# for row_num, vin in enumerate(vin_list):
#     print(vin)
#     df_xml= df[df.VIN_NUM.str.contains(vin)]
#     try:
#         xml_data= df_xml[df_xml.XML.to_list()[0]]
#     except Exception as e:
#         xml_data=soap_post(vin_num=vin)
#     if row_num==0:
#         filename= "vauto_test_data_GC(may16,2021).xlsx"
#     else:
#         filename= "test_data_XML(may18,2021).xlsx"
#     from openpyxl import load_workbook
#     wb = load_workbook(filename= filename)
#     ws = wb.worksheets[0]
#     ws[f"B{row_num+2}"]= xml_data

#     filename= "test_data_XML(may18,2021).xlsx"
#     wb.save(filename)
#     wb.close()