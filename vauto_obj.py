import imp
import os
import time
from click import option
from selenium.webdriver.common.keys import Keys
from decorator_time import time_decorater
from helper import send_alert
from vauto_config import DEALER_NAME, VAUTO_BOOK_NAME, VAUTO_PWD, VAUTO_URL, VAUTO_USERNAME, PRICE_XPATH
import logging, configparser, traceback

for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)

logging.basicConfig(filename='RO_Bot_app.log',
                    format='%(asctime)s %(levelname)s-%(message)s-%(message)s',
                    filemode='a',
                    level = logging.INFO)

class VautoAutomation:

    def __init__(self, driver, vin, odometer, config, model,percent_reduced_carfax,
                    cust_first_name= "", cust_last_name= "", phone_no=""):
        self.url= VAUTO_URL
        self.user_name= VAUTO_USERNAME
        self.password= VAUTO_PWD
        self.VAUTO_BOOK_NAME= VAUTO_BOOK_NAME
        self.PRICE_XPATH= PRICE_XPATH
        self.data_from_vauto_dict= dict()
        self.driver= driver
        self.vin= vin
        self.odometer= odometer
        self.model= model
        self.config= config
        self.percent_reduced_carfax= percent_reduced_carfax
        self.cust_first_name= cust_first_name
        self.cust_last_name= cust_last_name
        self.phone_no= phone_no
        self.click = lambda element: self.driver.execute_script("arguments[0].click();", element)
        self.popup_xpath= "//button[contains(text(), 'Remind Me')]"
    
    @time_decorater
    def vauto_login(self):
        self.driver.get(self.url)
        self.driver.find_element_by_xpath('//form//input[@id="username"]').send_keys(self.user_name)
        self.click(self.driver.find_element_by_xpath("//form//div//button[contains(text(), 'Next')]"))
        self.driver.find_element_by_xpath('//form//input[@id="password"]').send_keys(self.password)
        self.click(self.driver.find_element_by_xpath("//form//div//button[contains(text(), 'Sign')]"))
        time.sleep(3)
        self.driver.get(self.url)
        time.sleep(7)
        self.closing_poup()
        return self.driver.current_window_handle

    @time_decorater
    def fill_details(self):
        self.driver.find_element_by_xpath('//*[@id="Vin"]').send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_xpath('//*[@id="Vin"]').send_keys(Keys.DELETE)
        self.driver.find_element_by_xpath('//*[@id="Vin"]').send_keys(self.vin)
        self.driver.find_element_by_xpath('//*[@id="Odometer"]').send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_xpath('//*[@id="Odometer"]').send_keys(Keys.DELETE)
        self.driver.find_element_by_xpath('//*[@id="Odometer"]').send_keys(self.odometer) 
        time.sleep(3)
        self.driver.find_element_by_xpath('//*[@id="Vin"]').send_keys(Keys.ENTER)
        self.closing_poup()
        try:
            self.click(self.driver.find_element_by_xpath('//*[@id="ed8a0b24-aad6-30ff-4a0a-fb8948de5007"]')) #Closing Notification
        except Exception as e:
            print("Line 54: Ignore", e)
        try:
            self.click(self.driver.find_element_by_xpath("//em//button[contains(text(), 'Ignore')]")) #Closing Notification
        except Exception as e:
            print("Line 58: Ignore", e)
        self.driver.find_element_by_xpath('//*[@id="CustFirstName"]').send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_xpath('//*[@id="CustFirstName"]').send_keys(Keys.DELETE)
        self.driver.find_element_by_xpath('//*[@id="CustFirstName"]').send_keys(self.cust_first_name)

        self.driver.find_element_by_xpath('//*[@id="CustLastName"]').send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_xpath('//*[@id="CustLastName"]').send_keys(Keys.DELETE)
        self.driver.find_element_by_xpath('//*[@id="CustLastName"]').send_keys(self.cust_last_name)

        self.driver.find_element_by_xpath('//*[@id="CustHomePhone"]').send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_xpath('//*[@id="CustHomePhone"]').send_keys(Keys.DELETE)
        self.driver.find_element_by_xpath('//*[@id="CustHomePhone"]').send_keys(self.phone_no)

        self.driver.find_element_by_xpath("//input[@type= 'checkbox'][@id='LeaseReturn']").click()
        self.closing_poup()

    @time_decorater
    def select_service_name(self, service_name):
        #! Below lines are for selecting the source
        self.driver.find_element_by_xpath("//input[@id='VehicleSource']").send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_xpath("//input[@id='VehicleSource']").send_keys(Keys.DELETE)
        self.driver.find_element_by_xpath("//input[@id='VehicleSource']").send_keys(service_name.split())
        time.sleep(2)
        self.click(self.driver.find_element_by_xpath(f"//div[contains(text(), '{service_name}')]"))
    
    def closing_poup(self):
        try:
            self.click(self.driver.find_element_by_xpath(self.popup_xpath))
        except Exception as e:
            print("(Closing popup) Line 61: Ignore", e)

    @time_decorater
    def expand_book(self):
        book_title_ele= self.driver.find_element_by_xpath(f"//div[@class='x-panel-header x-unselectable']//span[contains(text(), '{VAUTO_BOOK_NAME}')]//parent::div//parent::div")
        if "collapsed" in book_title_ele.get_attribute('class').lower():
            self.click(self.driver.find_element_by_xpath(f"//div[@class='x-panel-header x-unselectable']//span[contains(text(), '{VAUTO_BOOK_NAME}')]"))
        else:
            logging.info(f"{VAUTO_BOOK_NAME} tab is already opened")
        self.driver.execute_script("arguments[0].scrollIntoView();", self.driver.find_element_by_xpath(f"//div[@class='x-panel-header x-unselectable']//span[contains(text(), '{VAUTO_BOOK_NAME}')]//parent::div//parent::div//parent::div//parent::td//parent::tr/following-sibling::tr"))
        self.driver.save_screenshot(os.path.join("screenshot", f"before_{self.vin}.png"))


    @time_decorater
    def reloading_elems(self):
        fields_to_extract= []
        for title_name_ele in self.driver.find_elements_by_xpath(f'//span[contains(text(), "{self.VAUTO_BOOK_NAME}")]//parent::div/following-sibling::div//div//h2[@class="optionFieldLabel"]'):
            fields_to_extract.append(title_name_ele.text.replace(":", ""))
        return fields_to_extract

    @time_decorater
    def get_selected_not_selected_options(self, operation_at):
        try:
            time.sleep(5)
            all_title_options_dict= {}
            for title in self.reloading_elems():
                all_title_options_dict[title]= {}
                if title== "Equipment":
                    options_list= self.driver.find_elements_by_xpath(f"//h2[contains(text(), '{title}')]//parent::div//ul//li//span//span//a")
                else:
                    options_list= self.driver.find_elements_by_xpath(f"//span[contains(text(), '{VAUTO_BOOK_NAME}')]//parent::div/following-sibling::div//div[@class= 'fields']//div[@class=' optionField']//h2[contains(text(), '{title}')]/following-sibling::div//ul//li//span//span//a")
                options_unchecked= [option for option in options_list if "checked" not in option.get_attribute("class")]
                options_checked= [option for option in options_list if "checked" in option.get_attribute("class")]
                for unchecked_ele in options_unchecked:
                    all_title_options_dict[title].update({unchecked_ele.text: "Not selected"})
                for checked_ele in options_checked:
                    all_title_options_dict[title].update({checked_ele.text: "Selected"})
                self.data_from_vauto_dict[operation_at]= all_title_options_dict
        except Exception as e: 
            logging.exception(e)

    @time_decorater
    def select_option_in_book(self, title, option_to_select):
        try:
            if title== "Equipment":
                options_list= self.driver.find_elements_by_xpath(f"//h2[contains(text(), '{title}')]//parent::div//ul//li//span//span//a")
            else:
                options_list= self.driver.find_elements_by_xpath(f"//span[contains(text(), '{self.VAUTO_BOOK_NAME}')]//parent::div/following-sibling::div//div[@class= 'fields']//div[@class=' optionField']//h2[contains(text(), '{title}')]/following-sibling::div//ul//li//span//span//a")
            for ele in options_list:
                if ele.text== option_to_select:
                    print("Clicking the option", ele.text, "->", title)
                    self.click(ele)
                    time.sleep(2)
        except Exception as e:
            print(f"(select_option_in_book) Line no: 128, Error: {e}")

    @time_decorater
    def get_book_options_and_state(self, title):
        # options_list= self.driver.find_elements_by_xpath(f"//span[contains(text(), '{self.VAUTO_BOOK_NAME}')]//parent::div/following-sibling::div//div[@class= 'fields']//div[@class=' optionField']//h2[contains(text(), '{title}')]/following-sibling::div//ul//li//span//span//a")
        if title== "Equipment":
            options_list= self.driver.find_elements_by_xpath(f"//h2[contains(text(), '{title}')]//parent::div//ul//li//span//span//a")
        else:
            options_list= self.driver.find_elements_by_xpath(f"//span[contains(text(), '{self.VAUTO_BOOK_NAME}')]//parent::div/following-sibling::div//div[@class= 'fields']//div[@class=' optionField']//h2[contains(text(), '{title}')]/following-sibling::div//ul//li//span//span//a")
        options_unchecked= [option.text for option in options_list if "checked" not in option.get_attribute("class")]
        options_checked= [option.text for option in options_list if "checked" in option.get_attribute("class")]
        options_list= [option.text for option in options_list]
        return options_list, options_checked, options_unchecked

    @time_decorater
    def get_price(self):
        os.makedirs("screenshot", exist_ok= True)
        self.driver.save_screenshot(os.path.join("screenshot", f"check_Price_{self.vin}.png"))
        self.data_from_vauto_dict["image_path"]= os.path.join("screenshot", f"check_Price_{self.vin}.png")
        try:
            final_price= self.driver.find_element_by_xpath(PRICE_XPATH).text
            self.data_from_vauto_dict["Price"]= final_price.strip()
            return final_price
        except Exception as e:
            return "Price not found"
    @time_decorater
    def saving_data_vauto(self):
        if self.config["vauto_login_details"]["save_details"]=="yes":
            logging.info("Closing the vauto website")
            self.click(self.driver.find_element_by_xpath("//em//button[contains(text(), 'Actions')]"))
            time.sleep(1)
            self.click(self.driver.find_element_by_xpath('//*[@id="saveButton"]'))
            time.sleep(5)
            send_alert(subject="RO Bot Notification, saved the details in vauto", body= f"Saved for vin: {self.vin}", receiver="gowthamkrishna.rapid@gmail.com")
        else:
            logging.info("Closing the vauto website")
            self.click(self.driver.find_element_by_xpath("//em//button[contains(text(), 'Cancel')]"))
            time.sleep(1)
            self.click(self.driver.find_element_by_xpath("//em//button[contains(text(), 'No')]"))
            time.sleep(2)

    @time_decorater
    def saving_data_in_vauto(self, data):
        details_to_add= "LeadId:"+ str(data["LeadId"])
        details_to_add+= "\nVendor:"+ DEALER_NAME
        details_to_add+= "\nLead_Type:"+"RO BuyBack Lead"
        details_to_add+= "\nVIN:"+data.get("VIN", "")
        details_to_add+= "\nF_Name:"+ data.get("F_Name", "")
        details_to_add+= "\nL_Name:"+ data.get("L_Name", "")
        details_to_add+= "\nCell_Num:"+ data.get("Cell_Phone", "")
        details_to_add+= "\nEmail:"+ data.get("EMAIL", "")
        details_to_add+= "\nValue:"+data.get("Price", "")
        details_to_add+= "\nVehicle Details:"

    @time_decorater
    def close_VAuto_site(self):
        self.driver.quit()

